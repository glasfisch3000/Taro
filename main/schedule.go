package main

import (
	"time"

	"lelux.net/taro/friedolin"
)

func newSchedule(eventLink bool) Schedule {
	return Schedule{
		EventLink: eventLink,
		Days:      make([]ScheduleDay, 5),
		MinTime:   8 * 60,
		MaxTime:   14 * 60,
	}
}

type Schedule struct {
	EventLink bool
	Labels    []ScheduleLabel
	Days      []ScheduleDay
	MinTime   uint
	MaxTime   uint
}

type ScheduleLabel struct {
	From uint
	Hour uint
}

type ScheduleDay [][]ScheduleMeeting

type ScheduleMeeting struct {
	friedolin.Meeting
	EventId friedolin.EventId
	Name    string
	Striped bool
	Hash    float64

	From uint
	To   uint
}

func timeToUint(t friedolin.Time) uint {
	return uint(t.Hour)*60 + uint(t.Minute)
}

func (s *Schedule) Add(id friedolin.EventId, name string, index int, count int, striped bool, meeting friedolin.Meeting) {
	if meeting.State == friedolin.Cancelled {
		return
	}
	if meeting.FromTime == nil || meeting.ToTime == nil {
		return
	}

	m := ScheduleMeeting{
		Meeting: meeting,
		EventId: id,
		Name:    name,
		Striped: striped,
		From:    timeToUint(*meeting.FromTime),
		To:      timeToUint(*meeting.ToTime),
	}

	m.Hash = float64(index) / float64(count)

	if m.Online && m.Room == nil {
		m.Room = &friedolin.MeetingRoom{Name: "Online"}
	}

	if s.MinTime > m.From {
		s.MinTime = m.From
	}
	if s.MaxTime < m.To {
		s.MaxTime = m.To
	}

	weekday := (m.Weekday - time.Monday) % 7

cols:
	for i, col := range s.Days[weekday] {
		for _, colE := range col {
			if m.From < colE.To && m.To > colE.From {
				continue cols
			}
		}
		col = append(col, m)
		s.Days[weekday][i] = col
		return
	}

	s.Days[weekday] = append(s.Days[weekday], []ScheduleMeeting{m})
}

func (s *Schedule) Finish() {
	for h := s.MinTime / 60; h <= s.MaxTime/60; h++ {
		l := ScheduleLabel{From: (h * 60) - s.MinTime, Hour: h}
		s.Labels = append(s.Labels, l)
	}

	for _, day := range s.Days {
		for _, col := range day {
			for i, m := range col {
				m.From -= s.MinTime
				m.To -= s.MinTime
				col[i] = m
			}
		}
	}
	s.MaxTime -= s.MinTime
	s.MinTime = 0
}
