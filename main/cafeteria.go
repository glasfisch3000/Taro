package main

import (
	"lelux.net/taro/cafeteria"
)

type DietItem struct {
	Emoji    string
	Contains string
}

func dietInfo(d cafeteria.Diet) []DietItem {
	var items []DietItem

	if d&cafeteria.Fish != 0 {
		items = append(items, DietItem{Emoji: "🐟", Contains: "Fisch"})
	}
	if d&cafeteria.Pork != 0 {
		items = append(items, DietItem{Emoji: "🥓", Contains: "Schweinefleisch"})
	}
	if d&cafeteria.Beef != 0 {
		items = append(items, DietItem{Emoji: "🥩", Contains: "Rindfleisch"})
	}
	if d&cafeteria.Poultry != 0 {
		items = append(items, DietItem{Emoji: "🍗", Contains: "Geflügel"})
	}
	if d&cafeteria.Crustacean != 0 {
		items = append(items, DietItem{Emoji: "🦀", Contains: "Krebstiere"})
	}
	if d&cafeteria.AnimalGelatin != 0 {
		items = append(items, DietItem{Emoji: "🧫", Contains: "Gelatine"})
	}
	if d&cafeteria.AnimalRennet != 0 {
		items = append(items, DietItem{Emoji: "🧪", Contains: "tierisches Lab"})
	}
	if d&cafeteria.Carmine != 0 {
		items = append(items, DietItem{Emoji: "🩸", Contains: "Karmin"})
	}
	if d&cafeteria.Vegetarian == 0 && len(items) == 0 {
		items = append(items, DietItem{Emoji: "🍖", Contains: "Fleisch"})
	}

	if d&cafeteria.Milk != 0 {
		items = append(items, DietItem{Emoji: "🥛", Contains: "Milch"})
	}
	if d&cafeteria.ChickenEgg != 0 {
		items = append(items, DietItem{Emoji: "🥚", Contains: "Ei"})
	}

	if d&cafeteria.Vegetarian != 0 {
		if d&cafeteria.Honey != 0 {
			items = append(items, DietItem{Emoji: "🍯", Contains: "Honig"})
		}
		if d&cafeteria.Vegan == 0 && d&cafeteria.Waxed != 0 {
			items = append(items, DietItem{Emoji: "🕯️", Contains: "Bienenwachs"})
		}
		if d&cafeteria.Vegan == 0 && len(items) == 0 {
			items = append(items, DietItem{Emoji: "🍳", Contains: "tierische Produkte"})
		}
	}

	if d&cafeteria.Caffeine != 0 {
		items = append(items, DietItem{Emoji: "☕", Contains: "Koffein"})
	}
	if d&cafeteria.Alcohol != 0 {
		items = append(items, DietItem{Emoji: "🍷", Contains: "Alkohol"})
	}
	if d&cafeteria.Gluten != 0 {
		items = append(items, DietItem{Emoji: "🌾", Contains: "Gluten"})
	}

	return items
}
