package main

import (
	"embed"
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"sync"
	"time"
	"unicode"
	"unicode/utf8"

	"lelux.net/taro/cafeteria"
	"lelux.net/taro/friedolin"
)

var sessionCookie = "taro_session"

//go:embed *.html
var templsFS embed.FS

const (
	layoutHtml = "layout.html"
)

var funcs = template.FuncMap{
	"formatEventType": func(e friedolin.EventType) string {
		switch e {
		case friedolin.Exercise:
			return "Übung"
		case friedolin.Lecture:
			return "Vorlesung"
		case friedolin.Tutorial:
			return "Tutorium"
		case friedolin.LanguageCourse:
			return "Sprachkurs"
		case friedolin.LectureExercise:
			return "Vorlesung/Übung"
		case friedolin.Exam:
			return "Prüfung"
		case friedolin.Consultation:
			return "Fragen zur Prüfung"
		case friedolin.RepeatExam:
			return "Nachprüfung"
		default:
			return fmt.Sprintf("<%d>", e)
		}
	},
	"formatMonth": func(m time.Month) string {
		switch m {
		case time.January:
			return "Jan."
		case time.February:
			return "Feb."
		case time.March:
			return "März"
		case time.April:
			return "April"
		case time.May:
			return "Mai"
		case time.June:
			return "Juni"
		case time.July:
			return "Juli"
		case time.August:
			return "Aug."
		case time.September:
			return "Sept."
		case time.October:
			return "Okt."
		case time.November:
			return "Nov."
		case time.December:
			return "Dez."
		default:
			return m.String()
		}
	},
	"formatGrade": func(n float32) string {
		s := strconv.FormatFloat(float64(n), 'f', 1, 32)
		return strings.Replace(s, ".", ",", 1)
	},
	"formatMoney": func(m cafeteria.Price) string {
		s := strconv.FormatFloat(float64(m.Value), 'f', 2, 32)
		s = strings.Replace(s, ".", ",", 1)
		switch m.Currency {
		case "EUR":
			return s + " €"
		default:
			return s + " " + m.Currency
		}
	},
	"dietInfo": func(d cafeteria.Diet) []DietItem {
		return dietInfo(d)
	},
	"dietColor": func(d cafeteria.Diet) template.CSS {
		if d&cafeteria.Vegan != 0 {
			if d&cafeteria.Gluten == 0 {
				return "#3b82f6" // blue
			} else {
				return "#22c55e" // green
			}
		} else if d&cafeteria.Vegetarian != 0 {
			return "#f59e0b" // orange
		} else {
			return "#ef4444" // red
		}
	},

	"hashColor": hashColor,
	"html": func(h *friedolin.HTML) (template.HTML, error) {
		s, err := h.Render()
		return template.HTML(s), err
	},
}

func hashColor(l float64, chroma float64, hash float64) template.CSS {
	return template.CSS(fmt.Sprintf("oklch(%f %f %f)", l, chroma, -hash*360-55))
}

type Render struct {
	Domain  string
	Session *Session
	Data    interface{}
}

type Error struct {
	Title string
	Api   ApiError
	Code  int
}

func wrapErr(err error, part, path string, id uint) Error {
	switch err {
	case friedolin.ErrLoggedOut:
		return Error{
			"Abgemeldet",
			ApiError{"loggedOut", ""},
			http.StatusUnauthorized,
		}
	case friedolin.ErrFunctionalityUnavailable:
		return Error{
			"Funktionalität im Moment nicht verfügbar",
			ApiError{"functionalityUnavailable", ""},
			http.StatusServiceUnavailable,
		}
	case friedolin.ErrWrongCredentials:
		return Error{
			"Passwort oder Benutzername falsch",
			ApiError{"wrongCredentials", ""},
			http.StatusUnauthorized,
		}
	case friedolin.ErrUnavailable:
		return Error{
			"Friedolin nicht erreichbar",
			ApiError{"unavailable", ""},
			http.StatusBadGateway,
		}
	case friedolin.ErrNotExist:
		return Error{
			"Nicht gefunden",
			ApiError{"notExist", ""},
			http.StatusNotFound,
		}
	default:
		if err2, ok := err.(friedolin.UserError); ok {
			return Error{
				err2.Message,
				ApiError{"userError", err2.Message},
				http.StatusBadRequest,
			}
		} else {
			log.Printf("error handling %s %s/%d: %s", part, path, id, err)
			return Error{
				"Interner Fehler",
				ApiError{"internalError", ""},
				http.StatusInternalServerError,
			}
		}
	}
}

type Session struct {
	friedolin.Fridolin
	LoggedIn bool
	Info     friedolin.MyInfo
}

var sessions sync.Map

func (s *Session) Check() error {
	info, err := s.Fridolin.MyInfo()
	if err == nil {
		s.Info = info
	}
	return err
}

func (s *Session) bootstrap() bool {
	err := s.Check()
	if err == friedolin.ErrLoggedOut {
		return false
	} else if err != nil {
		log.Println("failed to fetch user info:", err)
	}
	return true
}

func loadTempl(path string) *template.Template {
	if path == "" {
		path = "index"
	}

	b, err := templsFS.ReadFile(path + ".html")
	if err != nil {
		panic(err)
	}

	pageTmpl := fmt.Sprintf("{{template \"%s\" .}}\n%s", layoutHtml, b)
	layoutTempl := template.Must(template.New(layoutHtml).Funcs(funcs).ParseFS(templsFS, layoutHtml, "schedule_cmp.html"))
	return template.Must(layoutTempl.New(path).Parse(pageTmpl))
}

var errTempl = loadTempl("error")

func Route(path string, withId bool, post func(s *Session, id uint, get, post url.Values) (string, interface{}, error), get func(s *Session, id uint) (interface{}, error)) {
	fullPath := "/" + path
	if withId {
		fullPath += "/"
	}

	templ := loadTempl(path)

	http.HandleFunc(fullPath, func(w http.ResponseWriter, r *http.Request) {
		var id uint
		if withId {
			var err error
			id64, err := strconv.ParseUint(strings.TrimPrefix(r.URL.Path, fullPath), 10, 32)
			if err != nil {
				http.Error(w, "missing id suffix", http.StatusBadRequest)
				return
			}
			id = uint(id64)
		}

		domain := r.Host
		i := strings.IndexAny(r.Host, ":.")
		if i != -1 {
			domain = domain[:i]
		}

		var domainName strings.Builder
		for _, r := range domain {
			l := utf8.RuneLen(r)
			domain = domain[l:]

			tr := unicode.ToTitle(r)
			tl := utf8.RuneLen(tr)

			domainName.Grow(tl + len(domain))
			domainName.WriteRune(tr)
			domainName.WriteString(domain)
			break
		}
		domain = domainName.String()

		var s *Session
		cookie, cErr := r.Cookie(sessionCookie)
		if cErr == nil && cookie.Value != "" {
			sAny, loaded := sessions.LoadOrStore(cookie.Value, &Session{
				Fridolin: friedolin.Fridolin{Session: cookie.Value},
				LoggedIn: true})
			s = sAny.(*Session)
			if !loaded && !s.bootstrap() {
				*s = Session{}
				s = &Session{}
			}
		} else {
			s = &Session{}
		}

		w.Header().Set("Content-Type", "text/html; charset=utf-8")

		var data interface{}
		switch r.Method {
		case http.MethodGet:
			if get != nil {
				var err error
				data, err = get(s, id)
				switch err {
				case nil:
				case friedolin.ErrLoggedOut:
					http.Redirect(w, r, "/login?redirect="+url.QueryEscape(r.URL.RequestURI()), http.StatusTemporaryRedirect)
					return
				default:
					wErr := wrapErr(err, "get", path, id)

					w.WriteHeader(wErr.Code)
					err := errTempl.Execute(w, Render{domain, s, wErr})
					if err != nil {
						log.Printf("error executing error template %s/%d: %s", path, id, err)
					}
					return
				}
			}
		case http.MethodPost:
			if post != nil {
				err := r.ParseForm()
				if err != nil {
					http.Error(w, "failed to parse form", http.StatusBadRequest)
					return
				}

				var url string
				url, data, err = post(s, id, r.URL.Query(), r.PostForm)
				if err != nil {
					log.Printf("error handling post %s/%d: %s", path, id, err)
					http.Error(w, "internal error", http.StatusInternalServerError)
					return
				}
				if s.Session != "" {
					sessions.Store(s.Session, s)
				}
				http.SetCookie(w, &http.Cookie{
					Name:     sessionCookie,
					Value:    s.Session,
					Path:     "/",
					Secure:   true,
					HttpOnly: true,
					MaxAge:   24 * 60 * 60,
				})

				if url != "" {
					http.Redirect(w, r, url, http.StatusSeeOther)
					return
				} else {
					break
				}
			}
			fallthrough
		default:
			http.Error(w, "unsupported method", http.StatusMethodNotAllowed)
		}

		err := templ.Execute(w, Render{domain, s, data})
		if err != nil {
			log.Printf("error executing template %s/%d: %s", path, id, err)
		}
	})
}

type ApiError struct {
	Code    string `json:"code"`
	Message string `json:"message"`
}

func ApiRoute(postOnly bool, path string, withId bool, handle func(f friedolin.Fridolin, id uint, values url.Values) (interface{}, error)) {
	fullPath := "/api/" + path
	if withId {
		fullPath += "/"
	}

	http.HandleFunc(fullPath, func(w http.ResponseWriter, r *http.Request) {
		var id uint
		if withId {
			var err error
			id64, err := strconv.ParseUint(strings.TrimPrefix(r.URL.Path, fullPath), 10, 32)
			if err != nil {
				http.Error(w, "missing id suffix", http.StatusBadRequest)
				return
			}
			id = uint(id64)
		}

		if postOnly && r.Method != http.MethodPost {
			http.Error(w, "only post requests supported", http.StatusMethodNotAllowed)
			return
		}

		w.Header().Set("Content-Type", "application/json; charset=utf-8")

		q := r.URL.Query()

		var f friedolin.Fridolin
		session, ok := strings.CutPrefix(r.Header.Get("Authorization"), "Bearer ")
		if ok {
			f.Session = session
		} else {
			f.Session = q.Get("session")
		}
		f.Language = q.Get("language")

		data, err := handle(f, id, q)
		if err != nil {
			err2 := wrapErr(err, "api", path, id)
			w.WriteHeader(err2.Code)
			data = err2.Api
		}

		e := json.NewEncoder(w)
		e.SetEscapeHTML(false)
		err = e.Encode(data)
		if err != nil {
			log.Printf("failed to encode json: %s", err)
			http.Error(w, "internal error", http.StatusInternalServerError)
		}
	})
}
