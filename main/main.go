package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strings"
	"time"

	"lelux.net/taro/cafeteria"
	"lelux.net/taro/cafeteria/thueringen"
	"lelux.net/taro/friedolin"
)

type Event struct {
	friedolin.Event

	Schedule Schedule
	Module   friedolin.EventModule
	Moodle   string
}

type Module struct {
	friedolin.Module

	Events []friedolin.SearchEvent
}

type GradeModule struct {
	friedolin.GradeModule
	Exams []GradeExam
}

type GradeExam struct {
	friedolin.GradeExam
	Name  string
	Tries uint8
}

type Room struct {
	friedolin.Room
	Building friedolin.Building
}

type Cafeteria struct {
	Name string
	cafeteria.Cafeteria
}

type CafeteriaMeal struct {
	Name   string
	Dishes []cafeteria.Dish
}

type ApiLogin struct {
	Session string `json:"session"`
}

func main() {
	Route("", false, nil, nil)

	ApiRoute(true, "login", false, func(f friedolin.Fridolin, id uint, values url.Values) (interface{}, error) {
		err := f.Login(values.Get("username"), values.Get("password"))
		return ApiLogin{f.Session}, err
	})
	Route("login", false, func(s *Session, id uint, get, post url.Values) (string, interface{}, error) {
		err := s.Login(post.Get("username"), post.Get("password"))
		if err != nil {
			return "", nil, err
		}
		s.LoggedIn = true

		s.bootstrap()

		redirect := get.Get("redirect")
		if !strings.HasPrefix(redirect, "/") {
			redirect = "/schedule"
		}

		return redirect, nil, nil
	}, nil)

	ApiRoute(true, "logout", false, func(f friedolin.Fridolin, id uint, values url.Values) (interface{}, error) {
		return nil, f.Logout()
	})
	Route("logout", false, func(s *Session, id uint, get, post url.Values) (string, interface{}, error) {
		s.Logout()
		*s = Session{}
		return "/", nil, nil
	}, nil)

	ApiRoute(false, "schedule", false, func(f friedolin.Fridolin, id uint, values url.Values) (interface{}, error) {
		return f.Schedule()
	})
	Route("schedule", false, nil, func(s *Session, id uint) (interface{}, error) {
		fs, err := s.Schedule()
		if err != nil {
			return nil, err
		}

		rs := newSchedule(true)
		for i, e := range fs {
			for _, g := range e.Groups {
				for _, m := range g.Meetings {
					if m.Frequency != friedolin.Once {
						if m.Type == 0 {
							m.Type = e.Type
						}
						rs.Add(e.Id, e.Name, i, len(fs), e.Type == friedolin.Exercise, m)
					}
				}
			}
		}
		rs.Finish()

		return rs, nil
	})

	ApiRoute(false, "grades", true, func(f friedolin.Fridolin, id uint, values url.Values) (interface{}, error) {
		return f.Grades(uint8(id))
	})
	Route("grades", true, nil, func(s *Session, id uint) (interface{}, error) {
		fg, err := s.Grades(uint8(id))
		if err != nil {
			return nil, err
		}

		tries := make(map[uint32]uint8)
		for _, fs := range fg {
			for _, fm := range fs.Modules {
				for _, fe := range fm.Exams {
					if n, ok := tries[fe.Number]; !ok || n < fe.Try {
						tries[fe.Number] = fe.Try
					}
				}
			}
		}

		var g []GradeModule
		for _, fs := range fg {
			for _, fg := range fs.Modules {
				e := make([]GradeExam, len(fg.Exams))
				for i, fe := range fg.Exams {
					name := fg.Name
					if len(fe.Units) > 0 && len(fe.Units[0].Events) > 0 {
						name2, ok := strings.CutPrefix(fe.Units[0].Events[0].Name, fg.Name+" - ")
						if ok {
							name = name2
						}
					}

					e[i] = GradeExam{
						GradeExam: fe,
						Name:      name,
						Tries:     tries[fe.Number],
					}
				}

				g = append(g, GradeModule{GradeModule: fg, Exams: e})
			}
		}

		return g, nil
	})

	ApiRoute(false, "event", true, func(f friedolin.Fridolin, id uint, values url.Values) (interface{}, error) {
		return f.Event(friedolin.EventId(id))
	})
	Route("event", true, nil, func(s *Session, id uint) (interface{}, error) {
		fe, err := s.Event(friedolin.EventId(id))
		if err != nil {
			return nil, err
		}
		e := Event{Event: fe}

		e.Schedule = newSchedule(false)
		for i, g := range e.Groups {
			var name string
			if g.Number == nil {
				name = "unbenannte Gruppe"
			} else {
				name = fmt.Sprintf("Gruppe %d", *g.Number)
			}

			for _, m := range g.Meetings {
				e.Schedule.Add(fe.Id, name, i, len(e.Groups), m.Frequency == friedolin.Once, m)
			}
		}
		e.Schedule.Finish()

		for _, m := range e.Modules {
			if m.ShortText == e.ShortText {
				e.Module = m
			}
		}

		for _, link := range e.Links {
			if strings.HasPrefix(link.Link, "https://moodle.uni-jena.de/") {
				e.Moodle = link.Link
			}
		}

		return e, nil
	})

	ApiRoute(false, "module", true, func(f friedolin.Fridolin, id uint, values url.Values) (interface{}, error) {
		return f.Module(friedolin.ModuleId(id))
	})
	Route("module", true, nil, func(s *Session, id uint) (interface{}, error) {
		fm, err := s.Module(friedolin.ModuleId(id))
		if err != nil {
			return nil, err
		}
		m := Module{Module: fm}

		search, err := s.SearchEvents(friedolin.EventSearch{
			Name: fm.Name,
			Term: friedolin.Term{Season: friedolin.Summer, Year: 2024},
		}, 10, 0)
		if err != nil {
			return nil, err
		}

		for _, e := range search {
			if e.Name == fm.Name {
				m.Events = append(m.Events, e)
			}
		}

		return m, nil
	})

	ApiRoute(false, "room", true, func(f friedolin.Fridolin, id uint, values url.Values) (interface{}, error) {
		return f.Room(friedolin.RoomId(id))
	})
	Route("room", true, nil, func(s *Session, id uint) (interface{}, error) {
		r, err := s.Room(friedolin.RoomId(id))
		if err != nil {
			return nil, err
		}

		b, err := s.Building(r.Building.Id)
		if err != nil {
			log.Printf("failed to load building '%d': %s", r.Building.Id, err)
		}

		return Room{r, b}, nil
	})

	ApiRoute(false, "buildings", false, func(f friedolin.Fridolin, id uint, values url.Values) (interface{}, error) {
		return f.Buildings()
	})
	ApiRoute(false, "building", true, func(f friedolin.Fridolin, id uint, values url.Values) (interface{}, error) {
		return f.Building(friedolin.BuildingId(id))
	})

	ApiRoute(false, "myInfo", false, func(f friedolin.Fridolin, id uint, values url.Values) (interface{}, error) {
		return f.MyInfo()
	})

	ApiRoute(false, "paymentInformation", false, func(f friedolin.Fridolin, id uint, values url.Values) (interface{}, error) {
		return f.PaymentInformation()
	})

	ApiRoute(false, "cafeteria", true, func(f friedolin.Fridolin, id uint, values url.Values) (interface{}, error) {
		var t time.Time
		dateStr := values.Get("date")
		if dateStr == "" {
			t = time.Now()
		} else {
			time.Parse(dateStr, time.DateOnly)
		}

		c := thueringen.Thüringen{Id: uint8(id)}
		return c.Meals(context.Background(), t)
	})

	cs := []Cafeteria{
		{"Ernst-Abbe-Platz", thueringen.Jena_ErstAbbePlatz},
		{"Philosophenweg", thueringen.Jena_Philosophenweg},
		{"Uni Hauptgebäude", thueringen.Jena_UniHauptgebäude},
	}
	Route("cafeteria", false, nil, func(s *Session, id uint) (interface{}, error) {
		var res [5][]CafeteriaMeal

		for _, c := range cs {
			meals, err := c.Cafeteria.Meals(context.Background(), time.Now())
			if err != nil {
				return nil, err
			}
			for _, m := range meals {
				res[m.Time] = append(res[m.Time], CafeteriaMeal{
					Name:   c.Name,
					Dishes: m.Dishes,
				})
			}
		}

		return res, nil
	})

	fmt.Println("listening")
	fmt.Println("error:", http.ListenAndServe(":8080", nil))
}
