package friedolin

import (
	"fmt"
	"net/url"
	"strconv"
	"strings"

	"lelux.net/x/html"
)

type ModuleId uint

type Module struct {
	Id        ModuleId `json:"id"`
	Number    uint32   `json:"number"`
	Name      string   `json:"name"`
	ShortText string   `json:"shortText"`

	Credits         uint8     `json:"credits"`
	Cycle           TermCycle `json:"cycle"`
	WorkloadPrivate uint16    `json:"workloadPrivate"`
	WorkloadClass   uint16    `json:"workloadClass"`

	Content            *HTML `json:"content"`
	Literature         *HTML `json:"literature"`
	LearningObjectives *HTML `json:"learningObjectives"`
	ExamPrerequisites  *HTML `json:"examPrerequisites"`
}

type ModuleRequirement struct {
	ShortText string `json:"shortText"`
	Name      string `json:"name"`
}

func (m Module) Workload() uint16 {
	return m.WorkloadPrivate + m.WorkloadClass
}

func (f *Fridolin) Module(id ModuleId) (Module, error) {
	n, err := f.verpublish("PordDetail", "pord", "pord", url.Values{
		"pord.pordnr": {strconv.FormatUint(uint64(id), 10)},
	})
	if err != nil {
		return Module{}, err
	}

	m := Module{Id: id}

	firstTable := n.Query(table_)
	if firstTable == nil {
		return m, ErrNotExist
	}
	table := trTable(firstTable)

	var nameField string
	nameField, m.ShortText = table.TwoCols()
	i := strings.IndexByte(nameField, ']')
	m.Name = nameField[i+2:]
	number, err := strconv.ParseUint(nameField[1:i], 10, 32)
	if err != nil {
		return m, fmt.Errorf("failed to parse number: %w", err)
	}
	m.Number = uint32(number)
	table.Next()

	table.Next()

	_, creditsStr := table.TwoCols()
	credits, err := strconv.ParseUint(creditsStr, 10, 8)
	if err != nil {
		return m, fmt.Errorf("failed to parse points: %w", err)
	}
	m.Credits = uint8(credits)
	table.Next()

	table.Next()

	wlPrivateStr, cycleStr := table.TwoCols()
	wlPrivate, err := strconv.ParseUint(wlPrivateStr, 10, 16)
	if err != nil {
		return m, fmt.Errorf("failed to parse workload private: %w", err)
	}
	m.WorkloadPrivate = uint16(wlPrivate)
	switch cycleStr {
	case "jedes 2. Semester (ab Sommersemester)":
		m.Cycle = OnlySummer
	case "jedes 2. Semester (ab Wintersemester)":
		m.Cycle = OnlyWinter
	case "jedes Semester":
		m.Cycle = Every
	default:
		return m, fmt.Errorf("unknown term cycle '%s'", cycleStr)
	}
	table.Next()

	wlClassStr, _ := table.TwoCols()
	wlClass, err := strconv.ParseUint(wlClassStr, 10, 16)
	if err != nil {
		return m, fmt.Errorf("failed to parse workload class: %w", err)
	}
	m.WorkloadClass = uint16(wlClass)

	secondTable := (*html.Node)(firstTable.NextSibling.NextSibling)
	table = trTable(secondTable)

	for table.Next() {
		key := table.FirstChild.NextSibling
		inner := asHtml(key.NextSibling.NextSibling.FirstChild)
		switch key.FirstChild.Data {
		case "Content", "Inhalte":
			m.Content = inner
		case "Recommended reading", "Empfohlene Literatur":
			m.Literature = inner
		case "Intended learning outcomes", "Lern- und Qualifikationsziele":
			m.LearningObjectives = inner
		case "Voraussetzung für die Zulassung zur Modulprüfung":
			m.ExamPrerequisites = inner
		}
	}

	return m, nil
}
