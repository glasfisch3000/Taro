package friedolin

import (
	"bytes"
	"strings"
	"unicode"

	ghtml "golang.org/x/net/html"
	"lelux.net/x/html"
)

var (
	a            = html.MustCompile("a")
	hr           = html.MustCompile("hr")
	h1           = html.MustCompile("h1")
	table_       = html.MustCompile("table")
	tableSummary = html.MustCompile("table[summary]")
	tr           = html.MustCompile("tr")
	td           = html.MustCompile("td")
	form         = html.MustCompile("form")
	strong       = html.MustCompile("strong")
	object       = html.MustCompile("object")

	loginForm           = html.MustCompile(".loginform")
	userError           = html.MustCompile(".FSUusererror")
	loginStatusInternal = html.MustCompile(".FSUloginstatusinternal")
	nobr                = html.MustCompile(".nobr")
	internal            = html.MustCompile(".internal")
	regular             = html.MustCompile(".regular")
	exams1              = html.MustCompile("[headers=exams_1]")
	exams2              = html.MustCompile("[headers=exams_2]")
	contentTdLinks      = html.MustCompile(".contentTdLinks")
	colspan6            = html.MustCompile("[colspan='6']")
	li_treeList         = html.MustCompile("li.treelist")
	img_width100        = html.MustCompile("img[width='100']")
	name                = html.MustCompile("[name]")
)

type HTML html.Node

func asHtml(n *ghtml.Node) *HTML {
	if n.Type == ghtml.TextNode {
		n.Data = strings.TrimLeftFunc(n.Data, unicode.IsSpace)
	}

	c := n
	for {
		n.Parent = nil

		if c.NextSibling == nil {
			if c.Type == ghtml.TextNode {
				c.Data = strings.TrimRightFunc(c.Data, unicode.IsSpace)
			}
			return (*HTML)(n)
		} else {
			c = c.NextSibling
		}
	}
}

func (h *HTML) Render() (string, error) {
	var b strings.Builder
	for c := (*ghtml.Node)(h); c != nil; c = c.NextSibling {
		err := ghtml.Render(&b, c)
		if err != nil {
			return b.String(), err
		}
	}
	return b.String(), nil
}

func (h *HTML) MarshalText() ([]byte, error) {
	var b bytes.Buffer
	for c := (*ghtml.Node)(h); c != nil; c = c.NextSibling {
		err := ghtml.Render(&b, c)
		if err != nil {
			return b.Bytes(), err
		}
	}
	return b.Bytes(), nil
}

func naiveInnerHtml(b *strings.Builder, n *ghtml.Node) {
	for c := n; c != nil; c = c.NextSibling {
		if c.Type == ghtml.TextNode {
			b.WriteString(c.Data)
		} else {
			naiveInnerHtml(b, c.FirstChild)
		}
	}
}

type list struct {
	*html.Node
}

func (t *list) Next() bool {
	for t.Node != nil {
		t.Node = (*html.Node)(t.NextSibling)
		if t.Node != nil && t.Type == ghtml.ElementNode {
			return true
		}
	}
	return false
}

func (t *list) Yield() *html.Node {
	t.Next()
	return t.Node
}

func (t *list) TwoCols() (string, string) {
	first := t.Node.FirstChild.NextSibling.NextSibling.NextSibling
	firstStr := strings.TrimSpace(first.FirstChild.Data)

	secondHead := first.NextSibling.NextSibling
	if secondHead == nil {
		return firstStr, ""
	}
	second := secondHead.NextSibling.NextSibling
	if second.FirstChild == nil {
		return firstStr, ""
	}
	secondStr := strings.TrimSpace(second.FirstChild.Data)

	return firstStr, secondStr
}

func trTable(n *html.Node) list {
	return list{n.Query(tr)}
}
