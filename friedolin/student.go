package friedolin

import (
	"errors"
	"fmt"
	"io"
	"net/url"
	"strconv"
	"strings"

	"golang.org/x/net/html/atom"
	"lelux.net/x/html"
)

type MyInfo struct {
	FirstName           string `json:"firstName"`
	LastName            string `json:"lastName"`
	Username            string `json:"username"`
	MatriculationNumber uint32 `json:"matriculationNumber"`
}

func (f *Fridolin) MyInfo() (MyInfo, error) {
	err := f.fetchAsi()
	if err != nil {
		return MyInfo{}, err
	}

	n, err := f.htmlReq(false, "studienmatrixStudent", url.Values{
		"nextdir": {"qispos/studienmatrix/student"},
		"subitem": {"studienmatrixStudent"},
		"asi":     {f.Asi},
	})
	if err != nil {
		return MyInfo{}, err
	}

	var info MyInfo

	bar := n.Query(loginStatusInternal).FirstChild.NextSibling
	logout := bar.FirstChild.NextSibling
	nameEl := logout.NextSibling.NextSibling
	info.FirstName, info.LastName, _ = strings.Cut(nameEl.FirstChild.Data, "    ")
	loggedInAs := nameEl.NextSibling.NextSibling.FirstChild.Data
	info.Username = loggedInAs[strings.LastIndexByte(loggedInAs, ' ')+1:]

	text := n.Query(nobr).FirstChild.Data
	i := strings.LastIndexByte(text, '\u00a0')
	number, err := strconv.ParseUint(text[i+1:], 10, 32)
	info.MatriculationNumber = uint32(number)

	return info, err
}

func (f *Fridolin) DataSheet() (io.ReadCloser, error) {
	return f.pdfReq("verpublish", url.Values{
		"publishContainer": {"stammdaten"},
	})
}

func (f *Fridolin) EvaluationOfStudyProgress() (io.ReadCloser, error) {
	return f.pdfReq("verpublish", url.Values{
		"publishContainer": {"stammdatenib"},
	})
}

func (f *Fridolin) CertificateOfStudentStatus(term Term) (io.ReadCloser, error) {
	return f.pdfReq("verpublish", url.Values{
		"moduleCall":      {"Report"},
		"publishConfFile": {"studienbescheinigungSemester"},
		"publishSubDir":   {"qissosreports"},
		"publishid":       {strconv.FormatUint(uint64(term.id()), 10)},
	})
}

type PaymentInformation struct {
	Ticket           float32 `json:"ticket"`
	Studierendenwerk float32 `json:"studierendenwerk"`
	StudentCouncil   float32 `json:"studentCouncil"`

	Paid float32 `json:"paid"`
}

func (p PaymentInformation) Total() float32 {
	return p.Ticket + p.Studierendenwerk + p.StudentCouncil
}

func (p PaymentInformation) StillDue() float32 {
	return p.Total() - p.Paid
}

func (f *Fridolin) PaymentInformation() (PaymentInformation, error) {
	err := f.fetchAsi()
	if err != nil {
		return PaymentInformation{}, err
	}

	n, err := f.htmlReq(false, "gebkonto", url.Values{"asi": {f.Asi}})
	if err != nil {
		return PaymentInformation{}, err
	}

	links := n.QueryAll(contentTdLinks)
	if len(links) == 0 {
		return PaymentInformation{}, ErrFunctionalityUnavailable
	}

	var p PaymentInformation

	p.Ticket, err = parseCommaFloat(links[0].FirstChild.Data)
	if err != nil {
		return p, err
	}

	p.Studierendenwerk, err = parseCommaFloat(links[1].FirstChild.Data)
	if err != nil {
		return p, err
	}

	p.StudentCouncil, err = parseCommaFloat(links[2].FirstChild.Data)
	if err != nil {
		return p, err
	}

	p.Paid, err = parseCommaFloat(links[4].FirstChild.Data)

	return p, err
}

func parseCommaFloat(s string) (float32, error) {
	ds := strings.Replace(s, ",", ".", 1)
	n, err := strconv.ParseFloat(ds, 32)
	return float32(n), err
}

type MyEvent struct {
	Id     EventId
	Number uint32
	Name   string
}

func (f *Fridolin) MyEvents() ([]MyEvent, error) {
	err := f.fetchAsi()
	if err != nil {
		return nil, err
	}

	n, err := f.htmlReq(false, "wscheck", url.Values{"asi": {f.Asi}})
	if err != nil {
		return nil, err
	}

	fullRows := n.QueryAll(colspan6)
	res := make([]MyEvent, 0, len(fullRows))
	for _, row := range fullRows {
		a := (*html.Node)(row.FirstChild.NextSibling)
		if a == nil || a.GetAttr("class") != "regular" {
			continue
		}

		str := a.FirstChild.Data
		i := strings.IndexByte(str, ' ')
		if i == -1 {
			return res, errors.New("missing space in event name")
		}
		number, err := strconv.ParseUint(str[:i], 10, 32)
		if err != nil {
			return res, err
		}

		url, err := url.Parse(a.GetAttr("href"))
		if err != nil {
			return res, err
		}
		id, err := strconv.ParseUint(url.Query().Get("publishid"), 10, 32)
		if err != nil {
			return res, err
		}

		res = append(res, MyEvent{
			Id:     EventId(id),
			Number: uint32(number),
			Name:   str[i+1:],
		})
	}

	return res, nil
}

type ScheduleEvent struct {
	Id     EventId   `json:"id"`
	Number uint32    `json:"number"`
	Type   EventType `json:"type"`
	Name   string    `json:"name"`

	Term Term `json:"term"`

	Groups []Group `json:"groups"`
}

func (f *Fridolin) Schedule() ([]ScheduleEvent, error) {
	n, err := f.htmlReq(false, "wplan", url.Values{
		"show": {"liste"},
		"P.vx": {"lang"},
	})
	if err != nil {
		return nil, err
	} else if !loggedIn(n) {
		return nil, ErrLoggedOut
	}

	var res []ScheduleEvent

	seperator := n.Query(hr)

	for {
		headingField := seperator.NextSibling.NextSibling
		if headingField.DataAtom != atom.Div {
			break
		}

		a := (*html.Node)(headingField.FirstChild.NextSibling.FirstChild.NextSibling)
		url, err := url.Parse(a.GetAttr("href"))
		if err != nil {
			return res, err
		}
		id, err := strconv.ParseUint(url.Query().Get("publishid"), 10, 32)
		if err != nil {
			return res, err
		}
		e := ScheduleEvent{
			Id:   EventId(id),
			Name: strings.TrimSpace(a.FirstChild.Data),
		}

		descriptionField := headingField.NextSibling.NextSibling
		firstLine := descriptionField.FirstChild
		firstText := strings.TrimSpace(firstLine.Data)
		termStr, rest, ok := strings.Cut(firstText, "\u00a0\u00a0\u00a0")
		if !ok {
			return res, errors.New("missing nbsp after term")
		}
		e.Term, err = ParseTerm(termStr)
		if err != nil {
			return res, err
		}
		rest = strings.TrimSpace(rest)

		rest, ok = strings.CutPrefix(rest, "Nr.:\u00a0 ")
		if !ok {
			return res, errors.New("missing 'Nr.: ' before number")
		}
		numberStr, rest, ok := strings.Cut(rest, " \u00a0\u00a0\u00a0")
		if !ok {
			return res, errors.New("missing nbsp after number")
		}
		number, err := strconv.ParseUint(numberStr, 10, 32)
		if err != nil {
			return res, err
		}
		e.Number = uint32(number)
		rest = strings.TrimSpace(rest)

		typeStr, rest, ok := strings.Cut(rest, " \u00a0\u00a0")
		if !ok {
			return res, errors.New("missing nbsp after type")
		}
		e.Type, err = parseEventType(typeStr)

		seperator = (*html.Node)(descriptionField.NextSibling.NextSibling)

		for seperator.DataAtom == atom.Br {
			groupHeadingField := seperator.NextSibling.NextSibling
			groupTableField := (*html.Node)(groupHeadingField.NextSibling.NextSibling)

			g, err := parseGroup(groupTableField, false)
			if err != nil {
				return res, err
			}

			groupTitle := strings.TrimSpace(groupHeadingField.FirstChild.NextSibling.FirstChild.Data)
			_, groupTitle, ok = strings.Cut(groupTitle, " ")
			if !ok {
				return res, errors.New("missing space before group number")
			}
			if groupTitle != "[unbenannt]" {
				groupTitle, _, ok = strings.Cut(groupTitle, "-")
				if !ok {
					return res, errors.New("missing - after group number")
				}
				groupNumber, err := strconv.ParseUint(groupTitle, 10, 8)
				if err != nil {
					return res, err
				}
				number8 := uint8(groupNumber)
				g.Number = &number8
			}

			e.Groups = append(e.Groups, g)

			seperator = groupTableField
			for seperator.DataAtom != atom.Br && seperator.GetAttr("class") != "abstand_veranstaltung" {
				seperator = (*html.Node)(seperator.NextSibling.NextSibling)
			}
		}

		res = append(res, e)
	}

	return res, nil
}

type DescriptionModule struct {
	Id        ModuleId
	ShortText string
}

func (f *Fridolin) ModuleDescriptions(degree uint8, courseOfStudies uint16, mark string, version uint16) ([]DescriptionModule, error) {
	nodeId := fmt.Sprintf("auswahlBaum|abschluss:abschl=%d|studiengang:stg=%03d|stgSpecials:vert=,schwp=,kzfa=%s,pversion=%d", degree, courseOfStudies, mark, version)

	if f.Session != "" {
		err := f.fetchAsi()
		if err != nil {
			return nil, err
		}
	}

	n, err := f.htmlReq(false, "modulBeschrGast", url.Values{
		"nodeID": {nodeId},
		"asi":    {f.Asi},
	})
	if err != nil {
		return nil, err
	}

	var res []DescriptionModule

	list := list{Node: n.Query(li_treeList)}
	for list.Next() {
		a := list.Query(regular)
		shortText := a.FirstChild.FirstChild.Data

		url, err := url.Parse(a.GetAttr("href"))
		if err != nil {
			return nil, err
		}
		nodeId := url.Query().Get("nodeID")
		i := strings.LastIndexByte(nodeId, '=')
		if i == -1 {
			return nil, errors.New("missing = before module id")
		}
		id, err := strconv.ParseUint(nodeId[i+1:], 10, 32)
		if err != nil {
			return nil, err
		}
		m := DescriptionModule{Id: ModuleId(id), ShortText: shortText}

		res = append(res, m)
	}

	return res, nil
}
