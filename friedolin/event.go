package friedolin

import (
	"errors"
	"fmt"
	"log"
	"net/url"
	"strconv"
	"strings"
	"time"

	ghtml "golang.org/x/net/html"
	"lelux.net/x/html"
)

type EventId uint

type Event struct {
	Id        EventId     `json:"id"`
	Type      EventType   `json:"type"`
	Number    uint32      `json:"number"`
	ShortText string      `json:"shortText"`
	Name      string      `json:"name"`
	Links     []EventLink `json:"links"`

	Content       *HTML `json:"content"`
	Literature    *HTML `json:"literature"`
	Comment       *HTML `json:"comment"`
	Prerequisites *HTML `json:"prerequisites"`

	Term        Term      `json:"term"`
	Cycle       TermCycle `json:"cycle"`
	WeeklyHours float32   `json:"weeklyHours"`
	Members1    uint16    `json:"members1"`
	Members2    uint16    `json:"members2"`
	Credits     *uint8    `json:"credits"`

	Groups      []Group           `json:"groups"`
	Modules     []EventModule     `json:"modules"`
	Instructors []EventInstructor `json:"instructors"`
}

type EventLink struct {
	Title string `json:"title"`
	Link  string `json:"link"`
}

type Group struct {
	Number   *uint8    `json:"number"`
	Meetings []Meeting `json:"meetings"`
}

type Meeting struct {
	Type   EventType    `json:"type,omitempty"`
	Remark *HTML        `json:"remark"`
	Room   *MeetingRoom `json:"room"`
	Online bool         `json:"online"`
	State  MeetingState `json:"state,omitempty"`

	TimeOffset *uint8 `json:"timeOffset"`
	FromTime   *Time  `json:"fromTime"`
	ToTime     *Time  `json:"toTime"`

	Frequency Frequency    `json:"frequency"`
	Weekday   time.Weekday `json:"weekday"`
	FromDate  Date         `json:"fromDate"`
	ToDate    Date         `json:"toDate"`
}

type MeetingRoom struct {
	Id           RoomId `json:"id"`
	Name         string `json:"name"`
	BuildingName string `json:"buildingName"`
}

type MeetingState uint8

const (
	_ MeetingState = iota
	TakingPlace
	Cancelled
)

func (s MeetingState) String() string {
	switch s {
	case TakingPlace:
		return "TakingPlace"
	case Cancelled:
		return "Cancelled"
	default:
		return fmt.Sprintf("%%!MeetingState(%d)", s)
	}
}

func (r MeetingState) MarshalText() ([]byte, error) {
	switch r {
	case TakingPlace:
		return []byte("takingPlace"), nil
	case Cancelled:
		return []byte("cancelled"), nil

	default:
		return nil, fmt.Errorf("unknown meeting state %d", r)
	}
}

type EventInstructor struct {
	Id             PersonId       `json:"id"`
	Name           string         `json:"name"`
	Responsibility Responsibility `json:"responsibility,omitempty"`
}

type Responsibility uint8

const (
	_ Responsibility = iota
	Responsible
	Accompanying
	Organizational
)

func (r Responsibility) String() string {
	switch r {
	case Responsible:
		return "Responsible"
	case Accompanying:
		return "Accompanying"
	case Organizational:
		return "Organizational"
	default:
		return fmt.Sprintf("%%!Responsibility(%d)", r)
	}
}

func (r Responsibility) MarshalText() ([]byte, error) {
	switch r {
	case Responsible:
		return []byte("responsible"), nil
	case Accompanying:
		return []byte("accompanying"), nil
	case Organizational:
		return []byte("organizational"), nil
	default:
		return nil, fmt.Errorf("unknown responsiblity %d", r)
	}
}

func parseResponsibility(s string) (Responsibility, error) {
	switch s {
	case "":
		return 0, nil
	case "responsible", "verantwortlich":
		return Responsible, nil
	case "accompanying", "begleitend":
		return Accompanying, nil
	case "organizational", "organisatorisch":
		return Organizational, nil
	default:
		return 0, fmt.Errorf("unknown responsibility: '%s'", s)
	}
}

type EventModule struct {
	Id         ModuleId `json:"id"`
	ShortText  string   `json:"shortText"`
	Name       string   `json:"name"`
	ExamNumber uint32   `json:"examNumber"`
	UnitNumber uint32   `json:"unitNumber"`
}

func (f *Fridolin) Event(id EventId) (Event, error) {
	n, err := f.verpublish("webInfo", "webInfo", "veranstaltung", url.Values{
		"publishid": {strconv.FormatUint(uint64(id), 10)},
	})
	if err != nil {
		return Event{}, err
	}

	e := Event{Id: id}

	h1 := n.Query(h1)
	if h1 == nil {
		return e, ErrNotExist
	}
	heading := h1.FirstChild.Data
	headingEnd := strings.LastIndexByte(heading, '-')
	e.Name = heading[:headingEnd-1]

	sections := n.QueryAll(tableSummary)
	for _, section := range sections {
		switch section.GetAttr("summary") {
		case "Grunddaten zur Veranstaltung":
			failField, err := e.parseBaseInfo(trTable(section))
			if err != nil {
				return e, fmt.Errorf("failed to parse %s: %w", failField, err)
			}
		case "Übersicht über alle Veranstaltungstermine":
			g, err := parseGroup(section, true)
			if err != nil {
				return e, err
			}

			captionField := section.FirstChild.NextSibling
			caption := captionField.FirstChild.Data
			_, caption, ok := strings.Cut(caption, ": ")
			if !ok {
				return e, errors.New("missing ': ' before group number")
			}
			caption = strings.TrimSpace(caption)
			if caption != "[unbenannt]" {
				caption, _, ok = strings.Cut(caption, "-")
				if !ok {
					return e, errors.New("missing - after group number")
				}
				number, err := strconv.ParseUint(caption, 10, 8)
				if err != nil {
					return e, err
				}
				number8 := uint8(number)
				g.Number = &number8
			}

			e.Groups = append(e.Groups, g)
		case "Verantwortliche Dozenten":
			persons := trTable(section)
			for persons.Next() {
				nameCol := persons.FirstChild.NextSibling

				a := (*html.Node)(nameCol.FirstChild.NextSibling)
				link, err := url.Parse(a.GetAttr("href"))
				if err != nil {
					return e, fmt.Errorf("failed to parse person URL: %w", err)
				}
				idStr := link.Query().Get("personal.pid")
				if idStr != "" {
					id, err := strconv.ParseUint(idStr, 10, 32)
					if err != nil {
						return e, fmt.Errorf("failed to parse person id: %w", err)
					}
					name := strings.ReplaceAll(strings.TrimSpace(a.FirstChild.Data), "\u00a0", " ")
					i := EventInstructor{
						Id:   PersonId(id),
						Name: name}

					responsibilityCol := nameCol.NextSibling.NextSibling
					i.Responsibility, err = parseResponsibility(strings.TrimSpace(responsibilityCol.FirstChild.Data))
					if err != nil {
						return e, fmt.Errorf("failed to parse person responsibility: %w", err)
					}

					e.Instructors = append(e.Instructors, i)
				}
			}
		case "Übersicht über die zugehörigen Prüfungen":
			modules := trTable(section)
			for modules.Next() {
				var m EventModule

				cols := list{(*html.Node)(modules.FirstChild)}
				cols.Next()

				a := cols.Query(a)
				if a == nil {
					log.Printf("missing link in event %d module list", e.Id)
					continue
				}
				m.ShortText = strings.TrimSpace(a.FirstChild.Data)

				u, err := url.Parse(a.GetAttr("href"))
				if err != nil {
					return e, err
				}
				id, err := strconv.ParseUint(u.Query().Get("pord.pordnr"), 10, 32)
				if err != nil {
					return e, fmt.Errorf("failed to parse module id: %w", err)
				}
				m.Id = ModuleId(id)

				m.Name = strings.TrimSpace(modules.Query(exams2).FirstChild.Data)

				examStr := strings.TrimSpace(modules.Query(exams1).FirstChild.Data)
				i := strings.LastIndexByte(examStr, ':')
				examNumber, err := strconv.ParseUint(examStr[i+2:], 10, 32)
				if err != nil {
					return e, fmt.Errorf("failed to parse exam number: %w", err)
				}
				m.ExamNumber = uint32(examNumber)
				cols.Next()

				unit, err := strconv.ParseUint(cols.FirstChild.Data, 10, 32)
				if err != nil {
					return e, fmt.Errorf("failed to parse unit nuber: %w", err)
				}
				m.UnitNumber = uint32(unit)

				e.Modules = append(e.Modules, m)
			}
		case "Weitere Angaben zur Veranstaltung":
			contents := trTable(section)
			for {
				th := contents.FirstChild.NextSibling
				inner := asHtml(th.NextSibling.NextSibling.FirstChild)
				switch th.FirstChild.Data {
				case "Description", "Kommentar":
					e.Comment = inner
				case "Learning Content", "Lerninhalte":
					e.Content = inner
				case "Literature", "Literatur":
					e.Literature = inner
				case "Prerequisites", "Voraussetzungen":
					e.Prerequisites = inner
				}

				if !contents.Next() {
					break
				}
			}
		}
	}

	return e, nil
}

func parseGroup(n *html.Node, arrow bool) (Group, error) {
	var g Group

	meetings := trTable(n)
	for meetings.Next() {
		var m Meeting
		weekdayCol := meetings.FirstChild.NextSibling
		if arrow {
			weekdayCol = weekdayCol.NextSibling.NextSibling
		}
		weekdayField := strings.TrimSpace(weekdayCol.FirstChild.Data)
		switch weekdayField {
		case "Mo.", "Monday", "Montag":
			m.Weekday = time.Monday
		case "Tu.", "Di.", "Tuesday", "Dienstag":
			m.Weekday = time.Tuesday
		case "Wed.", "Mi.", "Wednesday", "Mittwoch":
			m.Weekday = time.Wednesday
		case "Th.", "Do.", "Thursday", "Donnerstag":
			m.Weekday = time.Thursday
		case "Fr.", "Friday", "Freitag":
			m.Weekday = time.Friday
		case "Sa.", "Saturday", "Samstag":
			m.Weekday = time.Saturday
		case "Su.", "So.", "Sunday", "Sonntag":
			m.Weekday = time.Sunday
		case "kA.", "n.s..":
			m.Weekday = -1
		default:
			return g, fmt.Errorf("unknown weekday: '%s'", weekdayField)
		}

		timeRangeCol := weekdayCol.NextSibling.NextSibling
		timeRangeText := timeRangeCol.FirstChild
		timeRangeBr := timeRangeText.NextSibling

		var fromTimeStr, toTimeStr string
		var ok bool
		if timeRangeBr == nil {
			fromTimeStr, toTimeStr, ok = strings.Cut(timeRangeText.Data, "\u00a0bis\u00a0")
			fromTimeStr = strings.TrimSpace(fromTimeStr)
			toTimeStr = strings.TrimSpace(toTimeStr)
		} else {
			fromTimeStr, ok = strings.CutSuffix(timeRangeText.Data, "\tbis")
			fromTimeStr = strings.TrimSpace(fromTimeStr)
			toTimeStr = strings.TrimSpace(timeRangeBr.NextSibling.Data)
		}

		if !ok {
			return g, fmt.Errorf("missing 'bis' in time range: '%s'", timeRangeText.Data)
		}

		if fromTimeStr != "" {
			fromTime, err := parseTime(fromTimeStr)
			if err != nil {
				return g, err
			}
			m.FromTime = &fromTime
		}

		if toTimeStr != "" {
			toTimeStr, quarter, ok := strings.Cut(toTimeStr, " ")
			if ok {
				switch quarter {
				case "s.t.":
					var o uint8 = 0
					m.TimeOffset = &o
				case "c.t.":
					var o uint8 = 15
					m.TimeOffset = &o
				default:
					log.Printf("unknown time offset '%s'", quarter)
				}
			}
			toTime, err := parseTime(toTimeStr)
			if err != nil {
				return g, err
			}
			m.ToTime = &toTime
		}

		frequencyCol := timeRangeCol.NextSibling.NextSibling
		frequencyField := strings.TrimSpace(frequencyCol.FirstChild.Data)
		frequencyField, _, _ = strings.Cut(frequencyField, "\n")
		switch frequencyField {
		case "w.", "weekly", "wöchentlich":
			m.Frequency = Weekly
		case "Einzel-V.", "single date", "Einzeltermin", "Block":
			m.Frequency = Once
		case "14d.", "14t.", "14-day", "14-täglich":
			m.Frequency = Fortnightly
		default:
			return g, fmt.Errorf("unknown frequency: '%s'", frequencyField)
		}

		durationCol := frequencyCol.NextSibling.NextSibling
		durationFrom := durationCol.FirstChild
		var err error
		m.FromDate, err = parseDate(strings.TrimSuffix(strings.TrimSpace(durationFrom.Data), "\u00a0bis"))
		if err != nil {
			return g, err
		}

		m.ToDate, err = parseDate(strings.TrimSpace(durationFrom.NextSibling.NextSibling.Data))
		if err != nil {
			return g, err
		}

		if !arrow {
			cancelledCol := durationCol.NextSibling.NextSibling
			lecturerCol := cancelledCol.NextSibling.NextSibling
			durationCol = lecturerCol
		}

		roomCol := durationCol.NextSibling.NextSibling
		roomA := (*html.Node)(roomCol.FirstChild.NextSibling)
		if roomA != nil {
			name := strings.TrimSpace(roomA.FirstChild.Data)
			if name != "" {
				if !arrow {
					name = strings.TrimSuffix(name, " (Hörsaal)")
					name = strings.TrimSuffix(name, " (Seminarraum)")
				}

				buildingName, name, _ := strings.Cut(name, " - ")

				u, err := url.Parse(roomA.GetAttr("href"))
				if err != nil {
					return g, err
				}
				id, err := strconv.ParseUint(u.Query().Get("raum.rgid"), 10, 32)
				if err != nil {
					return g, fmt.Errorf("failed to parse room id: %w", err)
				}

				m.Room = &MeetingRoom{
					Id:           RoomId(id),
					Name:         name,
					BuildingName: buildingName,
				}
			}
		}

		var statusField *ghtml.Node
		var remark *ghtml.Node

		if arrow {
			lecturerCol := roomCol.NextSibling.NextSibling

			statusCol := lecturerCol.NextSibling.NextSibling
			statusField = statusCol.FirstChild.NextSibling

			remark = statusCol.NextSibling.NextSibling.FirstChild
		} else {
			remarkCol := roomCol.NextSibling.NextSibling

			statusField = remarkCol.LastChild.PrevSibling
			if statusField != nil {
				switch (*html.Node)(statusField).GetAttr("class") {
				case "warnung", "grueneWarnung":
					brField := statusField.PrevSibling.PrevSibling
					if brField != nil {
						brField.PrevSibling.NextSibling = nil
						remark = remarkCol.FirstChild
					}
				default:
					statusField = nil
				}
			}
		}

		if statusField != nil {
			statusStr := statusField.FirstChild.Data
			switch statusStr {
			case "taking place", "findet statt":
				m.State = TakingPlace
			case "cancelled", "fällt aus":
				m.State = Cancelled
			default:
				return g, fmt.Errorf("unknown meeting status '%s'", statusStr)
			}
		}

		if remark != nil {
			var remarkText strings.Builder
			naiveInnerHtml(&remarkText, remark)
			remarkStr := strings.TrimSpace(remarkText.String())

			m.Type, err = parseEventType(remarkStr)
			if err != nil {
				if strings.EqualFold(strings.TrimRight(remarkStr, "!"), "Online") {
					m.Online = true
				} else {
					m.Remark = asHtml(remark)
				}
			}
		}

		g.Meetings = append(g.Meetings, m)
	}

	return g, nil
}

func (l *Event) parseBaseInfo(base list) (string, error) {
	typeStr, _ := base.TwoCols()
	var err error
	l.Type, err = parseEventType(typeStr)
	if err != nil {
		log.Println(err)
	}
	base.Next()

	var numberField string
	numberField, l.ShortText = base.TwoCols()
	if numberField != "" {
		number, err := strconv.ParseUint(numberField, 10, 32)
		l.Number = uint32(number)
		if err != nil {
			return "number", err
		}
	}
	base.Next()

	termField, whField := base.TwoCols()
	l.Term, err = ParseTerm(termField)
	if err != nil {
		return "term", err
	}
	if whField != "" {
		wh, err := strconv.ParseFloat(whField, 32)
		l.WeeklyHours = float32(wh)
		if err != nil {
			return "weekly hours", err
		}
	}
	base.Next()

	members1Field, members2Field := base.TwoCols()
	if members1Field != "" && !strings.HasPrefix(members1Field, "0 ") {
		members1, err := strconv.ParseUint(members1Field, 10, 16)
		l.Members1 = uint16(members1)
		if err != nil {
			return "members1", err
		}
	}
	if members2Field != "" {
		members2, err := strconv.ParseUint(members2Field, 10, 16)
		l.Members2 = uint16(members2)
		if err != nil {
			return "members2", err
		}
	}
	base.Next()

	frequency, _ := base.TwoCols()
	switch frequency {
	case "No take-over", "keine Übernahme":
		l.Cycle = NoRepetition
	case "Every 2nd term", "Jedes 2. Semester":
		l.Cycle = TermCycle(l.Term.Season)
	case "Every term", "Jedes Semester":
		l.Cycle = Every
	default:
		return "term cycle", fmt.Errorf("unknown frequency '%s'", frequency)
	}
	base.Next()

	creditsField, _ := base.TwoCols()
	if creditsField != "" {
		creditsField = strings.TrimSuffix(creditsField, " ECTs")
		credits, err := strconv.ParseUint(creditsField, 10, 8)
		if err != nil {
			return "credits", err
		}
		credits8 := uint8(credits)
		l.Credits = &credits8
	}
	base.Next()

	base.Next()

	base.Next()

	for _, a := range base.QueryAll(a) {
		_, title, ok := strings.Cut(a.GetAttr("title"), ": ")
		if !ok {
			return "link title", err
		}

		u, err := url.Parse(a.GetAttr("href"))
		if err != nil {
			return "link url", err
		}

		link := EventLink{
			Title: strings.TrimSpace(title),
			Link:  u.Query().Get("destination")}

		l.Links = append(l.Links, link)
	}

	return "", nil
}
