package friedolin

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"
)

type Frequency uint8

const (
	Once Frequency = iota
	Weekly
	Fortnightly
)

func (f Frequency) String() string {
	switch f {
	case Once:
		return "Once"
	case Weekly:
		return "Weekly"
	case Fortnightly:
		return "Fortnightly"
	default:
		return fmt.Sprintf("%%!Frequency(%d)", f)
	}
}

func (f Frequency) MarshalText() ([]byte, error) {
	switch f {
	case Once:
		return []byte("once"), nil
	case Weekly:
		return []byte("weekly"), nil
	case Fortnightly:
		return []byte("fortnightly"), nil
	default:
		return nil, fmt.Errorf("unknown frequency %d", f)
	}
}

type Time struct {
	Hour   uint8 `json:"hour"`
	Minute uint8 `json:"minute"`
}

func parseTime(s string) (Time, error) {
	hourStr, minuteStr, ok := strings.Cut(s, ":")
	if !ok {
		return Time{}, errors.New("missing colon")
	}

	hour, err := strconv.ParseUint(hourStr, 10, 8)
	if err != nil {
		return Time{}, fmt.Errorf("hour %w", err)
	}
	minute, err := strconv.ParseUint(minuteStr, 10, 8)
	if err != nil {
		return Time{}, fmt.Errorf("minute %w", err)
	}
	return Time{Hour: uint8(hour), Minute: uint8(minute)}, nil
}

type Date struct {
	Day   uint8      `json:"day"`
	Month time.Month `json:"month"`
	Year  uint16     `json:"year"`
}

func (d Date) format() string {
	return fmt.Sprintf("%02d.%02d.%d", d.Day, d.Month, d.Year)
}

func parseDate(s string) (Date, error) {
	dayStr, rest, ok := strings.Cut(s, ".")
	if !ok {
		return Date{}, errors.New("missing dot")
	}
	monthStr, yearStr, ok := strings.Cut(rest, ".")
	if !ok {
		return Date{}, errors.New("missing dot")
	}

	day, err := strconv.ParseUint(dayStr, 10, 8)
	if err != nil {
		return Date{}, fmt.Errorf("day %w", err)
	}
	month, err := strconv.ParseUint(monthStr, 10, 8)
	if err != nil {
		return Date{}, fmt.Errorf("month %w", err)
	}
	year, err := strconv.ParseUint(yearStr, 10, 16)
	if err != nil {
		return Date{}, fmt.Errorf("year %w", err)
	}

	return Date{Day: uint8(day), Month: time.Month(month), Year: uint16(year)}, nil
}
