package friedolin

import (
	"fmt"
	"log"
	"net/url"
	"strconv"
)

type ObjectId uint

type RoomId uint

type Room struct {
	Id             RoomId   `json:"id"`
	Type           RoomType `json:"type"`
	Name           string   `json:"name"`
	ShortName      string   `json:"shortName"`
	AdditionalName string   `json:"additionalName"`

	Building RoomBuilding `json:"building"`
	Images   []ObjectId   `json:"images"`
}

type RoomType uint8

const (
	LectureHall RoomType = iota + 1
	SeminarRoom
	Lab
	ComputerLab
	ReadingRoom
	Gym
	_
	DutyRoom
	MeetingRoom_
	OtherRoom
	_
	OutdoorFacility
	SwimmingPool
	SportsRoom
	CourseRoom
	_
	_
	TreatmentRoom
	Divers
	Foyer
	MultimediaCenter
	EventRoom
)

func (r RoomType) MarshalText() ([]byte, error) {
	switch r {
	case LectureHall:
		return []byte("lectureHall"), nil
	case SeminarRoom:
		return []byte("seminarRoom"), nil
	case Lab:
		return []byte("lab"), nil
	case ComputerLab:
		return []byte("computerLab"), nil
	case ReadingRoom:
		return []byte("readingRoom"), nil
	case Gym:
		return []byte("gym"), nil
	case DutyRoom:
		return []byte("dutyRoom"), nil
	case MeetingRoom_:
		return []byte("meetingRoom"), nil
	case OtherRoom:
		return []byte("otherRoom"), nil
	case OutdoorFacility:
		return []byte("outdoorFacility"), nil
	case SwimmingPool:
		return []byte("swimmingPool"), nil
	case SportsRoom:
		return []byte("sportsRoom"), nil
	case CourseRoom:
		return []byte("courseRoom"), nil
	case TreatmentRoom:
		return []byte("treatmentRoom"), nil
	case Divers:
		return []byte("divers"), nil
	case Foyer:
		return []byte("foyer"), nil
	case MultimediaCenter:
		return []byte("multimediaCenter"), nil
	case EventRoom:
		return []byte("eventRoom"), nil
	default:
		return nil, fmt.Errorf("unknown room type %d", r)
	}
}

type RoomBuilding struct {
	Id   BuildingId `json:"id"`
	Name string     `json:"name"`
}

func (f *Fridolin) Room(id RoomId) (Room, error) {
	n, err := f.verpublish("webInfo", "webInfoRaum", "raum", url.Values{
		"raum.rgid": {strconv.FormatUint(uint64(id), 10)},
	})
	if err != nil {
		return Room{}, err
	}

	r := Room{Id: id}

	table := n.Query(table_)
	if table == nil {
		return r, ErrNotExist
	}
	base := trTable(table)

	r.Name, _ = base.TwoCols()
	buildingA := base.Query(a)
	r.Building.Name = buildingA.FirstChild.Data
	url, err := url.Parse(buildingA.GetAttr("href"))
	if err != nil {
		return r, fmt.Errorf("failed to parse room building url: %w", err)
	}
	buildingId, err := strconv.ParseUint(url.Query().Get("k_gebaeude.gebid"), 10, 32)
	if err != nil {
		return r, fmt.Errorf("failed to parse room building id: %w", err)
	}
	r.Building.Id = BuildingId(buildingId)
	base.Next()

	r.ShortName, _ = base.TwoCols()
	base.Next()

	r.AdditionalName, _ = base.TwoCols()
	base.Next()

	kind, _ := base.TwoCols()
	switch kind {
	case "Hörsaal":
		r.Type = LectureHall
	case "Seminarraum":
		r.Type = SeminarRoom
	case "Labor":
		r.Type = Lab
	case "PC-Pool":
		r.Type = ComputerLab
	case "Lesesaal":
		r.Type = ReadingRoom
	case "Sporthalle":
		r.Type = Gym
	case "Dienstzimmer":
		r.Type = DutyRoom
	case "Besprechungsraum":
		r.Type = MeetingRoom_
	case "Sonstiger Raum":
		r.Type = OtherRoom
	case "Freianlage":
		r.Type = OutdoorFacility
	case "Schwimmhalle":
		r.Type = SwimmingPool
	case "Sportraum":
		r.Type = SportsRoom
	case "Kursraum":
		r.Type = CourseRoom
	case "Behandlungssaal":
		r.Type = TreatmentRoom
	case "Divers":
		r.Type = Divers
	case "Foyer":
		r.Type = Foyer
	case "MMZ":
		r.Type = MultimediaCenter
	case "Veranstaltungsraum":
		r.Type = EventRoom
	default:
		log.Printf("unknown room type '%s'", kind)
	}

	imgs := n.QueryAll(img_width100)
	r.Images = make([]ObjectId, len(imgs))
	for i, img := range imgs {
		url, err := url.Parse(img.GetAttr("src"))
		if err != nil {
			return r, fmt.Errorf("failed to parse photo url: %w", err)
		}
		id, err := strconv.ParseUint(url.Query().Get("objectid"), 10, 32)
		if err != nil {
			return r, fmt.Errorf("failed to parse object id: %w", err)
		}
		r.Images[i] = ObjectId(id)
	}

	return r, nil
}
