package friedolin

import (
	"fmt"
	"net/url"
	"strconv"
	"strings"

	"lelux.net/x/html"
)

type BuildingId uint

type Building struct {
	Id             BuildingId `json:"id"`
	Name           string     `json:"name"`
	ShortName      string     `json:"shortName"`
	AdditionalName string     `json:"additionalName"`

	Campus          string    `json:"campus"`
	AcademyBuilding string    `json:"academyBuilding"`
	Location        *Location `json:"location"`

	Rooms []BuildingRoom `json:"rooms"`
}

type Location struct {
	Lat float64 `json:"lat"`
	Lon float64 `json:"lon"`
}

type BuildingRoom struct {
	Id             RoomId `json:"id"`
	Name           string `json:"name"`
	ShortName      string `json:"shortName"`
	AdditionalName string `json:"additionalName"`
}

func (f *Fridolin) Building(id BuildingId) (Building, error) {
	n, err := f.verpublish("webInfo", "webInfoGeb", "gebaeude", url.Values{
		"expand":           {"Räume"},
		"k_gebaeude.gebid": {strconv.FormatUint(uint64(id), 10)},
	})
	if err != nil {
		return Building{}, err
	}

	b := Building{Id: id}

	tables := n.QueryAll(table_)
	if len(tables) == 0 {
		return b, ErrNotExist
	}

	for _, table := range tables {
		switch table.GetAttr("summary") {
		case "Grunddaten zum Gebäude":
			base := trTable(table)

			b.ShortName, b.Campus = base.TwoCols()
			base.Next()

			b.Name, b.AcademyBuilding = base.TwoCols()
			base.Next()

			b.AdditionalName, _ = base.TwoCols()
			base.Next()

			switch id {
			case 5: // Bachstrasse 18
				b.Location = &Location{50.92926, 11.57800}
			case 6: // Helmholtzweg 5
				b.Location = &Location{50.93444, 11.58255}
			case 131: // Fröbelstieg 1
				b.Location = &Location{50.93393, 11.58074}
			case 1831: // Carl-Zeiß-Straße 3
				b.Location = &Location{50.92867, 11.58156}
			default:
				mapEmbed := n.Query(object)
				if mapEmbed != nil {
					url, err := url.Parse(mapEmbed.GetAttr("data"))
					if err != nil {
						return b, fmt.Errorf("failed to parse map data: %w", err)
					}
					marker := url.Query().Get("marker")
					latStr, lonStr, ok := strings.Cut(marker, ",")
					if !ok {
						return b, fmt.Errorf("failed to split '%s' into lat and lon", marker)
					}
					lat, err := strconv.ParseFloat(latStr, 64)
					if err != nil {
						return b, fmt.Errorf("failed to parse lat: %w", err)
					}
					lon, err := strconv.ParseFloat(lonStr, 64)
					if err != nil {
						return b, fmt.Errorf("failed to parse lon: %w", err)
					}
					b.Location = &Location{lat, lon}
				}
			}
		case "Übersicht über alle Veranstaltungstermine":
			rooms := trTable(table)
			for rooms.Next() {
				cols := list{(*html.Node)(rooms.Node.FirstChild)}
				var r BuildingRoom

				cols.Next()
				shortNameField := (*html.Node)(cols.FirstChild.NextSibling)
				r.ShortName = shortNameField.FirstChild.Data
				url, err := url.Parse(shortNameField.GetAttr("href"))
				if err != nil {
					return b, fmt.Errorf("failed to parse building room url: %w", err)
				}
				id, err := strconv.ParseUint(url.Query().Get("raum.rgid"), 10, 32)
				if err != nil {
					return b, fmt.Errorf("failed to parse building room id: %w", err)
				}
				r.Id = RoomId(id)

				cols.Next()
				nameField := cols.FirstChild.NextSibling
				r.Name = strings.TrimSpace(nameField.FirstChild.Data)

				cols.Next()
				r.AdditionalName = strings.TrimSpace(cols.FirstChild.Data)

				b.Rooms = append(b.Rooms, r)
			}

		}
	}

	return b, nil
}

type BuildingsBuilding struct {
	Id   BuildingId `json:"id"`
	Name string     `json:"name"`
}

func (f *Fridolin) Buildings() ([]BuildingsBuilding, error) {
	n, err := f.htmlReq(false, "change", url.Values{
		"type":            {"6"},
		"moduleParameter": {"raumSelectGeb"},
		"next":            {"SearchSelect.vm"},
		"subdir":          {"raum"},
	})
	if err != nil {
		return nil, err
	}

	names := n.QueryAll(name)
	res := make([]BuildingsBuilding, len(names))

	for i, name := range names {
		url, err := url.Parse(name.GetAttr("href"))
		if err != nil {
			return res, err
		}
		id, err := strconv.ParseUint(url.Query().Get("raum.gebid"), 10, 32)
		if err != nil {
			return res, err
		}

		res[i] = BuildingsBuilding{
			Id:   BuildingId(id),
			Name: strings.TrimSpace(name.FirstChild.Data)}
	}

	return res, nil
}
