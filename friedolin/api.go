package friedolin

import (
	"errors"
	"io"
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"lelux.net/x/html"
	xhttp "lelux.net/x/net/http"
)

var (
	ErrLoggedOut                = errors.New("friedolin: logged out")
	ErrFunctionalityUnavailable = errors.New("friedolin: functionality currently unavailable")
	ErrWrongCredentials         = errors.New("friedolin: wrong credentials")
	ErrUnavailable              = errors.New("friedolin: server unavailable")
	ErrNotExist                 = errors.New("friedolin: does not exist")
)

type UserError struct {
	Message string
}

func (e UserError) Error() string {
	return "fridolin user error: " + e.Message
}

func (f *Fridolin) req(post bool, state string, values url.Values) (*http.Response, error) {
	method := http.MethodGet
	var body io.Reader
	if post {
		method = http.MethodPost
		body = strings.NewReader(values.Encode())
	}

	req, err := http.NewRequest(method, "https://friedolin.uni-jena.de/qisserver/rds", body)
	if err != nil {
		return nil, err
	}

	var q url.Values
	if !post {
		q = values
	}
	if q == nil {
		q = make(url.Values)
	}
	q.Set("state", state)
	if f.Language != "" {
		q.Set("language", f.Language)
	}
	req.URL.RawQuery = q.Encode()

	if values != nil {
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	}
	req.AddCookie(&http.Cookie{Name: "JSESSIONID", Value: f.Session})
	req.AddCookie(&http.Cookie{Name: "ROUTEID", Value: ".app01"})

	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	switch res.StatusCode {
	case http.StatusOK, http.StatusFound:
	case http.StatusServiceUnavailable:
		res.Body.Close()
		return nil, ErrUnavailable
	default:
		res.Body.Close()
		return nil, xhttp.UnexpectedStatus(res.Status)
	}

	for _, c := range res.Cookies() {
		if c.Name == "JSESSIONID" {
			f.Session = c.Value
		}
	}

	return res, nil
}

func (f *Fridolin) htmlReq(post bool, state string, values url.Values) (*html.Node, error) {
	res, err := f.req(post, state, values)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	n, err := html.Parse(res.Body)
	if err != nil {
		return nil, err
	}

	userErr := n.Query(userError)
	if userErr != nil {
		msg := userErr.FirstChild.NextSibling.FirstChild.Data
		return nil, UserError{msg}
	}

	if n.Query(loginForm) != nil {
		return nil, ErrLoggedOut
	}

	_ = f.extractAsi(n)

	return n, nil
}

func loggedIn(n *html.Node) bool {
	return n.Query(loginStatusInternal) != nil
}

func (f *Fridolin) extractAsi(n *html.Node) error {
	a := n.Query(internal)
	if a == nil {
		return errors.New("missing a tag")
	}

	url, err := url.Parse(a.GetAttr("href"))
	if err != nil {
		return err
	}
	f.Asi = url.Query().Get("asi")
	return nil
}

func (f *Fridolin) pdfReq(state string, values url.Values) (io.ReadCloser, error) {
	res, err := f.req(false, state, values)
	if err != nil {
		return nil, err
	}

	if res.Header.Get("Content-Type") != "application/pdf" {
		res.Body.Close()
		return nil, errors.New("fridolin: expected pdf")
	}

	return res.Body, nil
}

func (f *Fridolin) user(post bool, typ int, values url.Values) (*html.Node, error) {
	if values == nil {
		values = make(url.Values)
	}
	values.Set("type", strconv.Itoa(typ))
	return f.htmlReq(post, "user", values)
}

func (f *Fridolin) verpublish(moduleCall, confFile, subDir string, values url.Values) (*html.Node, error) {
	values.Set("moduleCall", moduleCall)
	values.Set("publishConfFile", confFile)
	values.Set("publishSubDir", subDir)
	return f.htmlReq(false, "verpublish", values)
}

func (f *Fridolin) verpublishContainer(container string, values url.Values) (*html.Node, error) {
	values.Set("publishContainer", container)
	return f.htmlReq(false, "verpublish", values)
}
