module lelux.net/taro

go 1.21.8

require (
	golang.org/x/net v0.5.0
	lelux.net/x v0.0.0-20230507130254-6b0313b1b0e9
)

require github.com/andybalholm/cascadia v1.3.1 // indirect
